import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import {
	User,
	Config
} from '@/common/api.js'
import { getUser, getToken } from '@/common/common.js'

Vue.use(Vuex)

const store = new Vuex.Store({
	getters,
	state: {
		systemInfo: null,
		appHide: false,
		userinfo: getUser(),
		path: '/pages/home/home',
		pusherMode: 0,
		config: null,
		video_list: [],
		nav_index: 0,
		token: getToken()
	},
	mutations: {
		SET_NAV_INDEX(state, index) {
			state.nav_index = index
		},
		setSystemInfo(state, data) {
			state.systemInfo = data
		},
		setAppHide(state, data) {
			state.appHide = data
		},
		setUserinfo(state, data) {
			state.userinfo = data
			uni.setStorage({
				key: 'userinfo',
				data,
				success: () => {}
			})
		},
		setPath(state, path) {
			state.path = path
		},
		setPusherMode(state, data) {
			state.pusherMode = data
		},
		setConfig(state, data) {
			state.config = data
		},
		setVideoList(state, data) {
			state.video_list = data
		}
	},
	actions: {
		updateUserinfo({ commit }) {
			User().then(({ code, data }) => {
				if (code === 200) {
					commit('setUserinfo', data)
				}
			})
		},
		getConfig({ commit }, key = 'base') {
			Config({ key }).then(({ code, data }) => {
				if (code === 200) {
					commit('setConfig', data)
				}
			})
		}
	}
})

export default store
