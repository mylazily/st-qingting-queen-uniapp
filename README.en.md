# 蜻蜓q软件是优雅草科技主打的软件产品，蜻蜓Q软件已经获得双软认证-蜻蜓Q旗舰版st-蜻蜓Q系列将成为优雅草科技的代名词衍生无数产品

### 介绍
蜻蜓Q旗舰版standard（简称stq）是优雅草科技2022点主打的一款涵盖直播，实时语音，朋友圈社交，IM聊天等的社交软件，此款多功能产品性能卓越，加载流畅稳定，安全级别高，前后端分离，后端框架为laravel+composer+前端vue+weex+uniapp+node.js的产品，使用多个原生sdk，可自由接入各类原生sdk，蜻蜓Q旗舰版在蜻蜓Q系统的基础上陆续发布更新内容，漫画，小说功能将逐步完善简介，章节，打赏等多个交互功能，蜻蜓Q旗舰版陆续增加直播礼物特效（分svg和mp4礼物）演示，直播群聊天，直播pk，直播单线连麦等多个 旗舰企业级功能，这是一款满足更快一步的个性化定制基础demo版本。

### 截图

#### 前端截图

![](https://doc.youyacao.com/server/index.php?s=/api/attachment/visitFile&sign=3a7517f746e654e15342efdca3b78cdb)
![](https://doc.youyacao.com/server/index.php?s=/api/attachment/visitFile&sign=aaf8888e2cf4bcaaa4a894a5987ad00b)
![](https://doc.youyacao.com/server/index.php?s=/api/attachment/visitFile&sign=9ccaae4dd03b878b5e4e6c16133868f5)
![](https://doc.youyacao.com/server/index.php?s=/api/attachment/visitFile&sign=f2e312c06739a7ebac7ce350e833ccfb)
![](https://doc.youyacao.com/server/index.php?s=/api/attachment/visitFile&sign=5c46e18a2e3ac64bc7294f2c9fd7500f)

![](https://doc.youyacao.com/server/index.php?s=/api/attachment/visitFile&sign=fcd555eb38bcc236f342cc3b391de90d)

![](https://doc.youyacao.com/server/index.php?s=/api/attachment/visitFile&sign=267d2158b683bf02feba0a1cebb2150f)

![](https://doc.youyacao.com/server/index.php?s=/api/attachment/visitFile&sign=871fb2cffba525191acf290a8ae25098)

#### 后台截图

![](https://doc.youyacao.com/server/index.php?s=/api/attachment/visitFile&sign=5635bd2354d9e81d3eacb4b03e4f7096)
![](https://doc.youyacao.com/server/index.php?s=/api/attachment/visitFile&sign=226b76abbd31b67a119d151bc5b01b47)
![](https://doc.youyacao.com/server/index.php?s=/api/attachment/visitFile&sign=a254dd994492661d1e8c092d13c215ef)

![](https://doc.youyacao.com/server/index.php?s=/api/attachment/visitFile&sign=67177df1a10b77339fa3dd14b31a3693)

![](https://doc.youyacao.com/server/index.php?s=/api/attachment/visitFile&sign=580dc7432da0b34791b2c03aed85ad9d)

![](https://fenfacun.youyacao.com/stq-mind.png)
### 演示

#### 前端APP演示

https://doc.youyacao.com/web/#/22/805

#### 后端演示

PHP蜻蜓第二版

[https://stqingtingadmin.youyacao.com/#/dashboard](https://stqingtingadmin.youyacao.com/#/dashboard)

demo 123456



### 蜻蜓系列软件架构

vue + nuve +uniapp +flutter + java + php +laravel

node.js +springboot2.2+springmvc+druid+mybatis+mangoDB+mysql+shiro-redis+redis+activemq

此库为蜻蜓q前端，蜻蜓q前端使用weex+vue+nuve+uniapp
 
 
 其他版本请关注松鼠/蜻蜓系统管网 

[松鼠短视频](https://songshu.youyacao.com/video.html)
[蜻蜓系统](https://songshu.youyacao.com/qingting.html)


### 文件目录说明

[蜻蜓系统官方目录说明](https://doc.youyacao.com/web/#/8?page_id=649)



### API接口文档

[点击查看API接口文档](https://doc.youyacao.com/web/#/16?page_id=93)





### 购买授权或技术支持

正版授权管网查询：

[点击查询授权以及购买其他技术支持服务](https://zhengban.youyacao.com)


授权客服企业Q联系:2853810243 官方QQ交流群：929353806  


### 国庆超级活动


0930举国欢庆：


神州奋起，国家繁荣;山河壮丽，岁月峥嵘;值此国庆佳节，祝愿我们伟大的祖国永远繁荣昌，助力中小企业恢复生产，提高中小企业竞争力，优雅草也依然献上自己的绵薄之力。

![](https://doc.youyacao.com/server/index.php?s=/api/attachment/visitFile&sign=3a11cfec342edd7648bd2d746d99d165)

超级爆炸活动政策如下：

原价49800元蜻蜓Q旗舰版现惊爆价只要。。。（请先看准入条件）



![](https://doc.youyacao.com/server/index.php?s=/api/attachment/visitFile&sign=93a7682314d247e7b23142fcd4691b82)


# 活动参加准入条件：

1，我国境内合法成立的政企单位，包含个体工商户，有限责任公司，股份有限公司，事业单位，党政机关。

2，给我们的gitee 点个小星星。

3，个人中心页最底部保留  技术支持·优雅草科技，（党政机关，事业单位如果不方便可去掉，其他单位需要保留，这是给优雅草的动力）

现把蜻蜓Q旗舰版前端代码已开放可浏览状态查看：

https://gitee.com/youyacao/st-qingting-queen-uniapp

如果觉得ok给我们来个小星星，感谢感谢~手动比心比心。

![](https://doc.youyacao.com/server/index.php?s=/api/attachment/visitFile&sign=47929f805cc3beaf1d7469bcc525b074)

![](https://doc.youyacao.com/server/index.php?s=/api/attachment/visitFile&sign=3d7fdbfbac5ca4c74af1803a0ec12c95)

![](https://doc.youyacao.com/server/index.php?s=/api/attachment/visitFile&sign=657a0cf082b4ed674742334689f5c4a3)


# 三个产品：

第一、原价49800元蜻蜓Q旗舰版，现惊爆价只要。。。服务器环境配置+系统搭建+云配置+杂项配置人工费，电子合同手续费，客服咨询人工成本费合计+2500元。【2500元拥有蜻蜓Q旗舰版st。】

第二、原价12800元蜻蜓K知识付费系统，现惊爆价只要。。。服务器环境配置+系统搭建+云配置+杂项配置人工费，电子合同手续费，客服咨询人工成本费合计+2500元。【2500元拥有蜻蜓Q旗舰版st。】

第三、原价19800元蜻蜓I即时通讯基础版，现惊爆价只要。。。服务器环境配置+系统搭建+云配置+杂项配置人工费，电子合同手续费，客服咨询人工成本费合计+goeasy通讯套餐基础费一年+5500元。【5500元拥有一套即时通讯软件。】

# 本活动有效期：

2022年10月1日-2022年12月30日（原计划一个月但是怕很多客户都还不知道活动就给结束了，因此为了真正帮扶到企业为中小企业提供便利，活动期整整3个月）


# 结尾

蜻蜓系列将在未来的三月保持密集更新，以上产品均针对软件，服务器劳烦广大客户们自行提供，也可联系优雅草获得优惠购买华为云或者腾讯云。

# 关于源代码

由于提供sass服务，因此暂时不提供源代码，党政机关，事业单位有安全考虑可以完整提供源代码进行私有化部署，涉密甲级涉密乙级企业单位，国企单位，央企单位同样也可提供源代码。

# 关于技术支持（支持1年）

由于技术支持需要大量人力物力，因此本活动的所有客户我们统一建立一个VIP群进行技术支持。