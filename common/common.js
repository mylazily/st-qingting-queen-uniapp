import { autoLogin } from '@/common/api.js'

export const getToken = () => {
	let token = null
	try {
		const _token = uni.getStorageSync('TOKEN')
		if (_token) {
			token = _token
		}
	} catch (e) {
		// error
	}
	return token
}

// 1维数组转2维数组
export const toTwoArray = (arr, count) => {
	let result = []
	let num = Math.ceil(arr.length / count);
	for (let i = 0; i < num; i++) {
		result.push(arr.slice(i * count, i * count + count))
	}
	return result
}

export const getUser = () => {
	let user = {
		avatar: '',
		username: '',
		nickname: '',
		desc: '',
		is_vip: 0,
		refcode: 'fMJzUY',
		birthday: ''
	}
	try {
		const _user = uni.getStorageSync('userinfo')
		if (_user) {
			user = {
				user,
				..._user
			}
		}
	} catch (e) {
		// error
	}
	return user
}

export const getStorage = (key) => {
	let value = null
	try {
	    value = uni.getStorageSync(key)
		return value
	} catch (e) {
	    // error
	}
	return value
}

export const authLoginFun = () => {
	const device_id = getStorage('device_id')
	autoLogin({ device_id }).then(({ code, data }) => {
		if (code === 200) {
			try {
			    uni.setStorageSync('TOKEN', data.token)
			} catch (e) {
			    // error
			}
		}
	})
}

export const setStorage = (key, value) => {
	try {
	    uni.setStorageSync(key, value)
	} catch (e) {
	    // error
	}
}
