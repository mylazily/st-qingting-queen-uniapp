import Vue from 'vue'
import App from './App'
import store from './store'
import uView from 'uview-ui'
import goeasy from '@/common/goeasy.js'

Vue.config.productionTip = false

Vue.use(uView)

Vue.prototype.goeasy = goeasy

App.mpType = 'app'

const app = new Vue({
	store,
	...App
})
app.$mount()
